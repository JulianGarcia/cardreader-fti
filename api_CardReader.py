from flask import Flask, render_template, request
from flask_restful import Resource, Api
import re


#Crear api
app = Flask(__name__)
api = Api(app)


class CardReader(Resource):

    visa = re.compile(r'^4\d{3}-?\d{4}-?\d{4}-?\d{4}$', flags=0)
    mastercard = re.compile(r'^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$', flags=0)
    americanexpress = re.compile(r'^3[4-7]\d{2}-?\d{4}-?\d{4}-?\d{4}$', flags=0)
    discover = re.compile(r'^6(011|44\d{1}|5\d{2})-?\d{4}-?\d{4}-?\d{4}$', flags=0)

    def is_mastercard(self, value):
        if CardReader.mastercard.match(str(value)):
            return True

    def is_visa(self, value):
        if CardReader.visa.match(str(value)):
            return True

    def is_americanexpress(self, value):
        if CardReader.americanexpress.match(str(value)):
            return True

    def is_discover(self, value):
        if CardReader.discover.match(str(value)):
            return True

    #Recibe un numero de tarjeta y devuelve un mensaje indicando de que tipo es.
    def analizar(self, card_number):
        if CardReader.is_mastercard(self, card_number):
            return "es mastercard"
        if CardReader.is_visa(self, card_number):
            return "es visa"
        if CardReader.is_americanexpress(self, card_number):
            return "es americanexpress"
        if CardReader.is_discover(self, card_number):
            return "es discover"
        else:
            return "No se corresponde con ningun tipo de tarjeta"        
    

#routes

#Recibe por request el numero de tarjeta y llama al metodo analizar con el numero de tarjeta.
#Luego muestra que tipo de tarjeta es.
@app.route("/", methods=["GET", "POST"])
def mostrar(card_number=None):    
    if request.method == 'POST':
        card_number = request.form['card_number']
        cr = CardReader()
        tipo = cr.analizar(card_number)
        return render_template("analizado.html", card_number = card_number, tipo_tarjeta = tipo)
    return render_template("index.html")


if __name__ == "__main__":
    app.run(debug=True)
