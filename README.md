Fundamentos Teóricos de Informática 2019
===================================
**Trabajo Práctico de Entrega N° 1**
--------------------------------

### Enunciado:
    
    Programe un extractor y verificador de números de tarjetas de crédito (como mínimo cuatro diferentes tarjetas)
    Por ejemplo:
        a) Las tarjetas Visa comienzan con 4, las nuevas tarjetas tienen 16 dígitos y las antiguas 13.
        b) Las tarjetas MasterCard comienzan con valores en el rango de 51 a 55 y todas cuentan con 16 dígitos.

#### Docentes
* Lic. Firmenich, Diego
* Lic. Almonacid, Samuel
* Lic. Navarro, Pablo

#### Integrantes
* Flores, Raúl
* García, Julián Ignacio
* Wheeler, Mariana

#### Como ejecutar
* Python 3.7
* Instalar flask
* Instalar flask-restful
* Ejecutar el siguiente comando:
    * *pyhton api_cardreader.py*

